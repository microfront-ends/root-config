import { registerApplication, start } from "single-spa";

//registerApplication({
//  name: "@single-spa/welcome",
//  app: () =>
//    System.import(
//      "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
//    ),
//  activeWhen: ["/"],
//});

// registerApplication({
//   name: "@sreasons/navbar",
//   app: () => System.import("@sreasons/navbar"),
//   activeWhen: ["/"]
// });

registerApplication({
   name: "@sreasons/mf-header",
   app: () => System.import("@sreasons/mf-header"),
   activeWhen: ["/"]
});

registerApplication({
  name: "@sreasons/mf-menu",
  app: () => System.import("@sreasons/mf-menu"),
  activeWhen: ["/"]
});

registerApplication({
  name: "@sreasons/mf-init",
  app: () => System.import("@sreasons/mf-init"),
  activeWhen: ["/documentos"]
});

registerApplication({
  name: "@sreasons/mf-shared-styles",
  app: () => System.import("@sreasons/mf-shared-styles"),
  activeWhen: ["/"]
});

registerApplication({
  name: "@sreasons/mf-ventas",
  app: () => System.import("@sreasons/mf-ventas"),
  activeWhen: ["/admin/ventas/apertura"]
});

start({
  urlRerouteOnly: true,
});